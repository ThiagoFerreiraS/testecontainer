package br.com.thiagoferreiras.testecontainer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import br.com.thiagoferreiras.testecontainer.adapter.StorieAdapter;
import br.com.thiagoferreiras.testecontainer.model.Character;
import br.com.thiagoferreiras.testecontainer.model.GlobalResponse;
import br.com.thiagoferreiras.testecontainer.service.MarvelService;
import br.com.thiagoferreiras.testecontainer.service.RestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterDetailsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private MarvelService mService;
    private RecyclerView mRecyclerView;
    private StorieAdapter mAdapter;
    private FrameLayout mProgress;
    private ScrollView mSvContent;
    private CardView mCvEmptyStories;
    private FrameLayout mFlTryAgain;
    private Button mBtnReload;
    private Integer mCharacterId;
    private String mHash = "2ed4f2080f37f03df6630da56bf13f80";
    private String mTs = "1477518770";

    private ImageView mImgCharacter;
    private TextView mTxtCharacterDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_details);

        Intent intent = getIntent();
        mCharacterId = intent.getIntExtra("characterId", 0);

        String title = intent.getStringExtra("title");

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        mImgCharacter = (ImageView) findViewById(R.id.img_character);
        mTxtCharacterDescription = (TextView) findViewById(R.id.txt_character_description);

        mService = RestService.getInstance().getService();
        mProgress = (FrameLayout) findViewById(R.id.fl_progress);
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mCvEmptyStories = (CardView) findViewById(R.id.cv_empyt_stories);
        mFlTryAgain = (FrameLayout) findViewById(R.id.fl_slow_connection);

        mBtnReload = (Button) findViewById(R.id.btn_reload);
        mBtnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgress.setVisibility(View.VISIBLE);
                mFlTryAgain.setVisibility(View.GONE);
                getCharacter();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_character_stories);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        getCharacter();
    }

    public void getCharacter(){
        mService.getCharacter(mCharacterId, mHash, mTs).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                if(response.isSuccessful()){
                    mProgress.setVisibility(View.GONE);
                    mSvContent.setVisibility(View.VISIBLE);

                    final float density = getResources().getDisplayMetrics().density;
                    int width = 150;
                    int h = (int) ((width * 1.5) * density + 0.5f);
                    width = (int) ((width) * density + 0.5f);

                    mImgCharacter.getLayoutParams().width = width;
                    mImgCharacter.getLayoutParams().height = h;
                    mImgCharacter.requestLayout();

                    Character character = response.body().getData().getResults().get(0);

                    Picasso.with(CharacterDetailsActivity.this)
                            .load(character.getThumbnail().getPath() + "/portrait_uncanny." + character.getThumbnail().getExtension())
                            .into(mImgCharacter, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

                    if(!character.getDescription().equals("")){
                        mTxtCharacterDescription.setText(character.getDescription());
                    }
                    else{
                        mTxtCharacterDescription.setText("Personegem sem descrição");
                    }

                    if(character.getStories().getItems().size() > 0){
                        mAdapter = new StorieAdapter(CharacterDetailsActivity.this, character.getStories().getItems());
                        mRecyclerView.setAdapter(mAdapter);
                    }
                    else{
                        mCvEmptyStories.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    mProgress.setVisibility(View.GONE);
                    mFlTryAgain.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                mProgress.setVisibility(View.GONE);
                mFlTryAgain.setVisibility(View.VISIBLE);
            }
        });
    }
}
