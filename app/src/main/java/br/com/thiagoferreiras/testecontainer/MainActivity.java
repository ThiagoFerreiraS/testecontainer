package br.com.thiagoferreiras.testecontainer;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import java.util.List;
import br.com.thiagoferreiras.testecontainer.adapter.CharacterAdapter;
import br.com.thiagoferreiras.testecontainer.clickHack.RecyclerViewOnClickListenerHack;
import br.com.thiagoferreiras.testecontainer.model.Character;
import br.com.thiagoferreiras.testecontainer.model.GlobalResponse;
import br.com.thiagoferreiras.testecontainer.service.MarvelService;
import br.com.thiagoferreiras.testecontainer.service.RestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack{

    private MarvelService mService;
    private RecyclerView mRecyclerView;
    private CharacterAdapter mAdapter;
    private FrameLayout mProgress;
    private List<Character> mList;
    private FrameLayout mFlTryAgain;
    private Button mBtnReload;
    private View mView;
    private Integer mOffset = 1;
    private Integer mLimit = 100;
    private static Integer DEFAULT_LIMIT = 100;
    private Integer mTotalCount;
    private boolean mRequestControl = false;
    private static String PUBLIC_API_KEY = "115da59916ca7f0edec0d7bde32a9d04";
    private static String PRIVATE_API_KEY = "";
    private String mHash = "2ed4f2080f37f03df6630da56bf13f80";
    private String mTs = "1477518770";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mView = findViewById(R.id.rl_parent);

        mService = RestService.getInstance().getService();
        mProgress = (FrameLayout) findViewById(R.id.fl_progress);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_character_list);
        mRecyclerView.setHasFixedSize(true);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(mList.size() == linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1){
                    Log.d("Thiago", "mList.size(): " + mList.size());
                    Log.d("Thiago", "mTotalCount: " + mTotalCount);
                    if(mList.size() < mTotalCount) {
                        if(mTotalCount - mList.size() <= DEFAULT_LIMIT){
                            mLimit = mTotalCount - mList.size();
                        }

                        if(!mRequestControl){
                            getMoreCharacters();
                        }
                    }
                    else{
                        Snackbar.make(mView, "Todos os personagens já foram carregados", Snackbar.LENGTH_INDEFINITE)
                                .setAction("Ok", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }).show();
                    }
                }
            }
        });

        mFlTryAgain = (FrameLayout) findViewById(R.id.fl_slow_connection);

        mBtnReload = (Button) findViewById(R.id.btn_reload);
        mBtnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgress.setVisibility(View.VISIBLE);
                mFlTryAgain.setVisibility(View.GONE);
                getCharacters();
            }
        });


//        mTs = System.currentTimeMillis()/1000;

//        mHash = MD5.encripty(mTs + PRIVATE_API_KEY + PUBLIC_API_KEY);

        Log.d("Thiago", "Hash: " + mHash);
        Log.d("Thiago", "Ts: " + mTs);

        getCharacters();

    }

    private void getCharacters(){
        mService.getCharacters(mHash, mTs, mOffset, mLimit).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                if(response.isSuccessful()){
                    Log.d("Thiago", "Success");
                    mOffset++;
                    mTotalCount = response.body().getData().getTotal();
                    mList = response.body().getData().getResults();
                    mAdapter = new CharacterAdapter(MainActivity.this, mList);
                    mAdapter.setRecyclerViewOnClickListenerHack(MainActivity.this);
                    mRecyclerView.setAdapter(mAdapter);
                    mProgress.setVisibility(View.GONE);
                }
                else{
                    mProgress.setVisibility(View.GONE);
                    mFlTryAgain.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                mProgress.setVisibility(View.GONE);
                mFlTryAgain.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getMoreCharacters(){
        mRequestControl = true;
        mAdapter.addProgressBar();
        mRecyclerView.scrollToPosition(mList.size() - 1);

        mService.getCharacters(mHash, mTs, mOffset, mLimit).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                mAdapter.removeProgressBar();
                if(response.isSuccessful()){
                    Log.d("Thiago", "Success");
                    mRequestControl = false;
                    mOffset++;
                    List<Character> characters = response.body().getData().getResults();

                    for(Character character : characters){
                        mAdapter.addListItem(character, mList.size());
                    }
                }
                else{
                    Snackbar.make(mView, "Não foi possível carregar mais personagens. Tente novamente.", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getMoreCharacters();
                                }
                            }).show();
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                mAdapter.removeProgressBar();
                Snackbar.make(mView, "Não foi possível carregar mais personagens. Tente novamente.", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getMoreCharacters();
                            }
                        }).show();
            }
        });
    }

    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(MainActivity.this, CharacterDetailsActivity.class);
        intent.putExtra("title", mList.get(position).getName());
        intent.putExtra("characterId", mList.get(position).getId());
        startActivity(intent);
    }
}
