package br.com.thiagoferreiras.testecontainer.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.thiagoferreiras.testecontainer.R;
import br.com.thiagoferreiras.testecontainer.clickHack.RecyclerViewOnClickListenerHack;
import br.com.thiagoferreiras.testecontainer.model.Character;

/**
 * Created by ThiagoFerreiraS on 26/10/2016.
 */
public class CharacterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mCtx;

    private LayoutInflater mLayoutInflater;
    private List<Character> mList;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Integer mProgressPosition = -1;

    public CharacterAdapter(Context ctx, List<Character> postList){
        this.mCtx = ctx;
        mList = postList;
        mLayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder mvh;
        if(viewType == 0) {
            View v = mLayoutInflater.inflate(R.layout.item_character, parent, false);
            mvh = new CharacterViewHolder(v);
        }else {
            View v = mLayoutInflater.inflate(R.layout.ll_progress_item, parent, false);
            mvh = new ProgressViewHolder(v);
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        } else if(holder instanceof CharacterViewHolder){
            ((CharacterViewHolder)holder).txtCharacterName.setText(mList.get(position).getName());
            Log.d("Thiago", mList.get(position).getThumbnail().getPath() + "/standard_large." + mList.get(position).getThumbnail().getExtension());
            Picasso.with(mCtx)
                    .load(mList.get(position).getThumbnail().getPath() + "/standard_large." + mList.get(position).getThumbnail().getExtension())
                    .into(((CharacterViewHolder)holder).imgCharacter, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return mList.isEmpty() ? 0 : mList.size();
    }

    public void addListItem(Character character, int position){
        mList.add(character);
        notifyItemInserted(position);
    }

    public void addProgressBar(){
        mList.add(null);
        Log.d("Thiago", "Setando posição do progress:" + String.valueOf(mList.size()-1));
        mProgressPosition = mList.size()-1;
        notifyItemInserted(mList.size());
    }

    public void removeProgressBar(){
        mList.remove(mList.size() - 1);
        mProgressPosition = -1;
        notifyItemRemoved(mList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        Log.d("Thiago", "Comparando posição:" + position);
        Log.d("Thiago", "Comparando posição:" + mProgressPosition);
        if (position == mProgressPosition) {
            return 1; //progress bar
        } else
            return 0; //item normal
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public class CharacterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imgCharacter;
        public TextView txtCharacterName;

        public CharacterViewHolder(View itemView) {
            super(itemView);

            imgCharacter = (ImageView) itemView.findViewById(R.id.img_character);
            txtCharacterName = (TextView) itemView.findViewById(R.id.txt_character_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getAdapterPosition());
            }
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar)v.findViewById(R.id.progress);
        }
    }
}
