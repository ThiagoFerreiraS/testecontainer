package br.com.thiagoferreiras.testecontainer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.thiagoferreiras.testecontainer.R;
import br.com.thiagoferreiras.testecontainer.model.Item;

/**
 * Created by yoopye on 27/10/2016.
 */
public class StorieAdapter extends RecyclerView.Adapter<StorieAdapter.StorieViewHolder> {

    private List<Item> mList;

    private Context mCtx;

    private LayoutInflater mLayoutInflater;
    private Integer mProgressPosition = -1;

    public StorieAdapter(Context ctx, List<Item> storieList){
        this.mCtx = ctx;
        mList = storieList;
        mLayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public StorieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_storie, parent, false);
        return new StorieViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StorieViewHolder holder, int position) {
        holder.txtStorieName.setText(mList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mList.isEmpty() ? 0 : mList.size();
    }

    public class StorieViewHolder extends RecyclerView.ViewHolder{
        public TextView txtStorieName;

        public StorieViewHolder(View itemView) {
            super(itemView);
            txtStorieName = (TextView) itemView.findViewById(R.id.txt_storie_name);
        }
    }
}
