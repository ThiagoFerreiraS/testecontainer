package br.com.thiagoferreiras.testecontainer.clickHack;

import android.view.View;

/**
 * Created by ThiagoFerreiraS on 26/10/2016.
 */
public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
