package br.com.thiagoferreiras.testecontainer.layout.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import br.com.thiagoferreiras.testecontainer.R;

/**
 * Created by ThiagoFerreiraS on 22/10/2016.
 */
public class FrameTryAgain extends FrameLayout{
    private TextView mTitle;
    private TextView mMessage;

    public FrameTryAgain(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.fl_try_again, this);

        mTitle = (TextView) findViewById(R.id.txt_title);
        mMessage = (TextView) findViewById(R.id.txt_message);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FrameSlowConnection);
        mTitle.setText(a.getString(R.styleable.FrameSlowConnection_android_title));
        mMessage.setText(a.getString(R.styleable.FrameSlowConnection_android_text));
        a.recycle();
    }
}
