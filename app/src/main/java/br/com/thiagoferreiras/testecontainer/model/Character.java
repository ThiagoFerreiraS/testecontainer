package br.com.thiagoferreiras.testecontainer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ThiagoFerreiraS on 26/10/2016.
 */
public class Character {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("modified")
    @Expose
    private String modified;

    @SerializedName("resourceURI")
    @Expose
    private String resourceURI;

    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;

    @SerializedName("comics")
    @Expose
    private Segment comics;

    @SerializedName("series")
    @Expose
    private Segment series;

    @SerializedName("stories")
    @Expose
    private Segment stories;

    @SerializedName("events")
    @Expose
    private Segment events;

    @SerializedName("urls")
    @Expose
    private List<Url> urls;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Segment getComics() {
        return comics;
    }

    public void setComics(Segment comics) {
        this.comics = comics;
    }

    public Segment getSeries() {
        return series;
    }

    public void setSeries(Segment series) {
        this.series = series;
    }

    public Segment getStories() {
        return stories;
    }

    public void setStories(Segment stories) {
        this.stories = stories;
    }

    public Segment getEvents() {
        return events;
    }

    public void setEvents(Segment events) {
        this.events = events;
    }

    public List<Url> getUrls() {
        return urls;
    }

    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }
}
