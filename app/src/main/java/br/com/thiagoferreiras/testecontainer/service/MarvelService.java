package br.com.thiagoferreiras.testecontainer.service;

import br.com.thiagoferreiras.testecontainer.model.GlobalResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ThiagoFerreiraS on 26/10/2016.
 */
public interface MarvelService {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })

    @GET("/v1/public/characters?apikey=115da59916ca7f0edec0d7bde32a9d04")
    Call<GlobalResponse> getCharacters (@Query("hash") String hash, @Query("ts") String ts, @Query("offset") Integer offset, @Query("limit") Integer limit);

    @GET("/v1/public/characters/{characterId}?apikey=115da59916ca7f0edec0d7bde32a9d04")
    Call<GlobalResponse> getCharacter (@Path("characterId") Integer characterID, @Query("hash") String hash, @Query("ts") String ts);

}
