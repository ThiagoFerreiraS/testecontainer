package br.com.thiagoferreiras.testecontainer.service;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ThiagoFerreiraS on 26/10/2016.
 */
public class RestService {

    private static RestService ourInstance = new RestService();

    public static RestService getInstance() {
        return ourInstance;
    }

    private MarvelService service;

    private RestService() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new LoggingInterception());
        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://gateway.marvel.com")
                .client(client)

                .build();

        service = retrofit.create(MarvelService.class);

    }

    public MarvelService getService ( ) {
        return service;
    }
}
